/* global BLUE, RED, SKY_BLUE, GREEN, PURPLE, vectorDoTwo, divide, MINIMUM_SPEED, MAXIMUM_SPEED, MINIMUM_RADIUS, MINIMUM_SIZE, times, UPDATE_SECOND, Audio, leeEnfieldSound, WORLD_SIZE, MINIMUM_SIZE */
/* eslint no-unused-vars: "off" */

const objects = []
const fixedObjects = []
const unfixedObjects = []
const createObject = (x, y, color, fixed = false) => {
  const object = {
    position: { x, y },
    draw: { x, y },
    color,
    movementRate: MINIMUM_SPEED,
    velocity: { x: 0, y: 0 },
    fixed,
    input: {
      left: false,
      right: false,
      up: false,
      down: false,
      aim: false,
      fire: false,
      strafeLeft: false,
      strafeRight: false
    }
  }

  objects.push(object)
  fixed ? fixedObjects.push(object) : unfixedObjects.push(object)
  return object
}

const leeEnfield = {
  accuracy: 3,
  cooldown: UPDATE_SECOND * 2,
  charge: 0,
  hit: {},
  ray: [],
  remaining: 0,
  sound: leeEnfieldSound,
  time: 0
}

const circles = []
const fixedCircles = []
const unfixedCircles = []
const createCircle = (x, y, color, radius, angle, fixed = false) => {
  const circle = createObject(x, y, color, fixed)
  circle.radius = radius
  circle.angle = angle
  circle.drawAngle = angle
  circle.weaponSize = circle.radius + MAXIMUM_SPEED
  circle.ai = { name: 'civilian' }

  circles.push(circle)
  fixed ? fixedCircles.push(circle) : unfixedCircles.push(circle)
  return circle
}

const rectangles = []
const fixedRectangles = []
const unfixedRectangles = []

const createRectangle = (
  x,
  y,
  color,
  width,
  height,
  fixed = false
) => {
  const rectangle = createObject(x, y, color, fixed)
  rectangle.width = width
  rectangle.halfWidth = width / 2
  rectangle.height = height
  rectangle.halfHeight = height / 2
  rectangle.size = { x: width, y: height }
  rectangle.halfSize = { x: rectangle.halfWidth, y: rectangle.halfHeight }

  rectangles.push(rectangle)
  fixed ? fixedRectangles.push(rectangle) : unfixedRectangles.push(rectangle)
  return rectangle
}

const getRandomPosition = () => ({
  x: Math.random() * (WORLD_SIZE - MINIMUM_SIZE) - ((WORLD_SIZE - MINIMUM_SIZE) / 2),
  y: Math.random() * (WORLD_SIZE - MINIMUM_SIZE) - ((WORLD_SIZE - MINIMUM_SIZE) / 2)
})

const createRandomCircle = () => {
  const position = getRandomPosition()
  const radius = (Math.random() * MINIMUM_RADIUS) + MINIMUM_RADIUS
  const angle = Math.random() * 2 * Math.PI

  return createCircle(position.x, position.y, BLUE, radius, angle)
}

const createRandomRectangle = () => {
  const position = getRandomPosition()
  const width = Math.random() * 100 + MINIMUM_SIZE
  const height = Math.random() * 100 + MINIMUM_SIZE

  return createRectangle(position.x, position.y, PURPLE, width, height, true)
}

const createRandomObject = () => {
  const shape = Math.random()
  if (shape > 0.5) {
    return createRandomCircle()
  } else {
    return createRandomRectangle()
  }
}
times(createRandomObject, 100)

const npcs = []
const combatants = []
const roamers = []
const createRoamer = () => {
  const roamer = createRandomCircle()
  roamer.ai = {
    angle: 0,
    getAngle () { return Math.random() * Math.PI * 2 },
    getTime () { return Math.random() * UPDATE_SECOND * 30 },
    movementRate: MAXIMUM_SPEED * (0.5 + Math.random() * 0.5),
    name: 'roamer',
    time: 0
  }
  roamer.angle = roamer.ai.getAngle()
  roamer.color = GREEN
  roamer.movementRate = MINIMUM_SPEED + Math.random() * MINIMUM_SPEED

  npcs.push(roamer)
  combatants.push(roamer)
  roamers.push(roamer)
  return roamer
}
times(createRoamer, 100)

const player = createCircle(0, 0, SKY_BLUE, MINIMUM_SIZE * 0.75, 0)
player.attack = leeEnfield
player.ai = { name: 'player' }
player.score = 0
player.movementRate = MAXIMUM_SPEED * 0.75
combatants.push(player)

const roamer1 = createRoamer()
roamer1.position = { x: 10, y: 10 }

const houseSize = MINIMUM_SIZE * 10
const halfHouseSize = houseSize / 2
createRectangle(
  halfHouseSize - MINIMUM_RADIUS,
  halfHouseSize + MINIMUM_SIZE * 2,
  PURPLE,
  houseSize,
  houseSize,
  true
) // The House

createRectangle(-500, 0, PURPLE, 10, 1000, true) // Left wall
createRectangle(500, 0, PURPLE, 10, 1000, true) // Right wall
createRectangle(0, 500, PURPLE, 1000, 10, true) // Top wall
createRectangle(0, -500, PURPLE, 1000, 10, true) // Bottom wall

// Initialize the camera
const camera = {
  position: {
    x: 50,
    y: 50
  },
  target: player,
  angle: player.drawAngle
}
