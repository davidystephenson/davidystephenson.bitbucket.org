/* global circles, rectangles, objects, getZeroVector, getUnitVector, getDirectionVector, player, getDirectionVector, getDistance, getSequence, getRay, BLACK, vectorDoTwo, vectorSubtract, vectorAdd, unfixedCircles, UPDATE_RATE_MODIFIER, MAXIMUM_SPEED, UPDATE_SECOND, unfixedRectangles, fixedCircles, vectorDifference, vectorDoThree, Audio, UPDATED_RATE, reloadSound, aimSound, hitSound, misfireSound, TWO_PI, roamers, civilianHitSound */
/* eslint no-unused-vars: "off" */

const hitCheckPoint = (point) => {
  const circle = circles.find(
    (circle) => getDistance(circle.position, point) < circle.radius
  )
  return circle || rectangles.find((rectangle) => {
    const difference = vectorDifference(point, rectangle.position)
    const violation = vectorSubtract(rectangle.halfSize, difference)
    return violation.x > 0 && violation.y > 0
  })
}

const rayCast = (ray) => {
  const pointer = {
    point: ray[ray.length - 1],
    object: null
  }
  ray.some((point) => {
    const hit = hitCheckPoint(point)
    if (hit) {
      pointer.point = point
      pointer.object = hit

      return true
    }
  })

  return pointer
}

const rectangleBounce = (violation, a, b) => {
  if (violation.y > violation.x) {
    if (a.position.x > b.position.x) {
      if (!a.fixed) a.position.x += MAXIMUM_SPEED
      if (!b.fixed) b.position.x -= MAXIMUM_SPEED
    } else {
      if (!a.fixed) a.position.x -= MAXIMUM_SPEED
      if (!b.fixed) b.position.x += MAXIMUM_SPEED
    }
  } else {
    if (a.position.y > b.position.y) {
      if (!a.fixed) a.position.y += MAXIMUM_SPEED
      if (!b.fixed) b.position.y -= MAXIMUM_SPEED
    } else {
      if (!a.fixed) a.position.y -= MAXIMUM_SPEED
      if (!b.fixed) b.position.y += MAXIMUM_SPEED
    }
  }
}

const circleRectangleBounce = (circle, rectangle) => {
  const difference = vectorDifference(
    circle.position, rectangle.position
 )
  const violation = vectorDoTwo(
    rectangle.halfSize, difference, (a, b) => circle.radius + a - b
  )
  const sideCase = Math.max(violation.x, violation.y) > circle.radius
  const sideCollision = violation.x > 0 && violation.y > 0 && sideCase

  const diagonal = {
    x: rectangle.halfSize.x,
    y: -rectangle.halfSize.y
  }
  const corners = [
    vectorAdd(rectangle.position, diagonal),
    vectorSubtract(rectangle.position, diagonal),
    vectorAdd(rectangle.position, rectangle.halfSize),
    vectorSubtract(rectangle.position, rectangle.halfSize)
  ].map(corner => getDistance(corner, circle.position))
  const cornerDistance = Math.min(...corners)

  const collision = sideCollision || (cornerDistance < circle.radius)

  if (collision) {
    rectangleBounce(violation, circle, rectangle)
  }
}

// Prepare reload sound
const playReload = () => reloadSound.play()
const reloadDelay = (player.attack.cooldown * UPDATED_RATE) - 1265

const update = () => {
  roamers.map(roamer => {
    if (roamer.ai.time < 0) {
      roamer.ai.angle = roamer.ai.getAngle()
      roamer.ai.time = roamer.ai.getTime()
    }

    if (roamer.angle % TWO_PI > roamer.ai.angle) {
      roamer.input.right = false
      roamer.input.left = true
    } else {
      roamer.input.right = true
      roamer.input.left = false
    }

    roamer.input.up = true
    roamer.ai.time -= 1
  })

  objects.map(object => {
    // Turn
    if (object.input.right) { object.angle += 0.05 }
    if (object.input.left) { object.angle -= 0.05 }

    // Move
    if (!object.input.aim) {
      object.velocity = getZeroVector()
      const forward = getDirectionVector(object.angle)
      const right = getDirectionVector(object.angle + Math.PI / 2)

      if (object.input.down) {
        object.velocity = vectorSubtract(object.velocity, forward)
      }
      if (object.input.up) {
        object.velocity = vectorAdd(object.velocity, forward)
      }
      if (object.input.strafeLeft) {
        object.velocity = vectorSubtract(object.velocity, right)
      }
      if (object.input.strafeRight) {
        object.velocity = vectorAdd(object.velocity, right)
      }

      object.velocity = getUnitVector(object.velocity)
      object.position = vectorDoTwo(
        object.position,
        object.velocity,
        (a, b) => a + (b * object.movementRate)
      )
    }
  })

  // Bounce
  unfixedCircles.map((circle, index) => {
    circles.map((innerCircle, innerCircleIndex) => {
      if (index !== innerCircleIndex) {
        const distance = getDistance(circle.position, innerCircle.position)
        if (distance < circle.radius + innerCircle.radius) {
          const unitVector = getUnitVector(
            vectorSubtract(circle.position, innerCircle.position)
          )

          circle.position = vectorDoTwo(
            circle.position, unitVector, (a, b) => a + b * MAXIMUM_SPEED
          )

          if (!innerCircle.fixed) {
            innerCircle.position = vectorDoTwo(
              innerCircle.position, unitVector, (a, b) => a - b * MAXIMUM_SPEED
            )
          }
        }
      }
    })

    rectangles.map((rectangle) => circleRectangleBounce(circle, rectangle))
  })

  unfixedRectangles.map((rectangle, index) => {
    rectangles.map((innerRectangle, innerRectangleIndex) => {
      if (index !== innerRectangleIndex) {
        const difference = vectorDifference(
          rectangle.position, innerRectangle.position
        )
        const violation = vectorDoThree(
          rectangle.halfSize,
          innerRectangle.halfSize,
          difference,
          (a, b, c) => a + b - c
        )

        if (violation.x > 0 && violation.y > 0) {
          rectangleBounce(violation, rectangle, innerRectangle)
        }
      }
    })

    fixedCircles.map((circle) => circleRectangleBounce(circle, rectangle))
  })

  player.attack.remaining = Math.max(0, player.attack.remaining - 1)
  if (player.attack.remaining === 0) {
    if (player.input.aim) {
      if (player.attack.charge === 0) { aimSound.play() }
      player.attack.charge += player.attack.accuracy
    } else if (player.input.fire) {
      player.input.fire = false
      player.attack.remaining = player.attack.cooldown
      const startPoint = {
        x: player.position.x + ((player.radius + 0.01) * Math.cos(player.angle)),
        y: player.position.y + ((player.radius + 0.01) * Math.sin(player.angle))
      }

      const sequence = getSequence(0, player.attack.charge, 1)
      player.attack.ray = getRay(startPoint, player.angle, sequence)

      player.attack.hit = rayCast(player.attack.ray)

      if (player.attack.hit.object && !player.attack.hit.object.fixed) {
        hitSound.play()
        if (player.attack.hit.object.ai.name === 'roamer') {
          if (player.attack.hit.object.color !== BLACK) {
            player.score += 1
          }
        } else if (player.attack.hit.object.ai.name === 'civilian') {
          civilianHitSound.play()
        }
        player.attack.hit.object.color = BLACK
      }
      player.attack.charge = 0
      player.attack.sound.play()
      setTimeout(playReload, reloadDelay)
    }
  } else {
    if (player.input.aim) { misfireSound.play() }
    player.input.fire = false
    player.attack.charge = 0
  }
}
