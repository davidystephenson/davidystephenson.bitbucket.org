/* global draw, update, UPDATED_RATE */

// Connect to the canvas
const canvas = document.getElementById('myCanvas')
const context = canvas.getContext('2d')
context.font = '5px Arial'

window.requestAnimFrame = ((callback) => {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  ((callback) => { window.setTimeout(callback, 1000 / 60) })
})()

const init = () => {
  draw()
  setInterval(update, UPDATED_RATE)
}

setTimeout(init, 200)
