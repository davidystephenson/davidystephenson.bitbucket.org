/* eslint no-unused-vars: "off" */

// Canvas
const CANVAS_WIDTH = 161.8
const CANVAS_HEIGHT = 100

// Colors
const RED = '#FF0000'
const SKY_BLUE = '#0088FF'
const BLUE = '#0000FF'
const BLACK = '#000000'
const LIME = '#00FF00'
const GREEN = '#008000'
const YELLOW = '#FFFF00'
const PURPLE = '#7F007F'
const FIRE_YELLOW = '#E5AD23'

// Update
const UPDATE_RATE = 50
const UPDATE_RATE_MODIFIER = 1
const UPDATED_RATE = UPDATE_RATE / UPDATE_RATE_MODIFIER
const UPDATE_SECOND = 1000 / UPDATED_RATE

// Size
const MINIMUM_SIZE = 5
const MINIMUM_RADIUS = MINIMUM_SIZE / 2

// Velocity
const MAXIMUM_SPEED = (MINIMUM_SIZE / 5) * UPDATE_RATE_MODIFIER
const MINIMUM_SPEED = (MAXIMUM_SPEED / 2)

// World
const WORLD_SIZE = 1000

//
const TWO_PI = Math.PI * 2
