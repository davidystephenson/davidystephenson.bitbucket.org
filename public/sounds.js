/* global Audio */
/* eslint no-unused-vars: "off" */

const leeEnfieldSound = new Audio('./public/sound/leeEnfield.mp3')
const aimSound = new Audio('./public/sound/holdBreath.mp3')
const reloadSound = new Audio('./public/sound/reload.wav')
const misfireSound = new Audio('./public/sound/misfire.wav')
const hitSound = new Audio('./public/sound/hit.mp3')
const civilianHitSound = new Audio('./public/sound/civilianHit.mp3')
