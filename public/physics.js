/* eslint no-unused-vars: "off" */

const add = (a, b) => a + b
const subtract = (a, b) => a - b
const divide = (a, b) => a / b
const multiply = (a, b) => a * b
const difference = (a, b) => Math.abs(a - b)

const vectorDo = (vector, fn) => ({ x: fn(vector.x), y: fn(vector.y) })
const vectorDoTwo = (a, b, fn) => (
  { x: fn(a.x, b.x), y: fn(a.y, b.y) }
)
const vectorDoThree = (a, b, c, fn) => (
  { x: fn(a.x, b.x, c.x), y: fn(a.y, b.y, c.y) }
)

const vectorAdd = (a, b) => vectorDoTwo(a, b, add)
const vectorSubtract = (a, b) => vectorDoTwo(a, b, subtract)
const vectorDivide = (a, b) => vectorDoTwo(a, b, divide)
const vectorMultiply = (a, b) => vectorDoTwo(a, b, multiply)
const vectorDifference = (a, b) => vectorDoTwo(a, b, difference)

const times = (fn, number, current = 0) => {
  fn()
  if (current < number) times(fn, number, current + 1)
}

const getLength = (vector) => Math.sqrt(
  Math.pow(vector.x, 2) + Math.pow(vector.y, 2)
)

const getDistance = (a, b) => getLength(vectorSubtract(a, b))

const getUnitVector = (vector) => {
  const vectorLength = getLength(vector)
  if (vectorLength === 0) return vector

  return vectorDo(vector, (value) => value / vectorLength)
}

const getZeroVector = () => ({ x: 0, y: 0 })

const getDirectionVector = (angle) => ({ x: Math.cos(angle), y: Math.sin(angle) })

const getSequence = (start, end, step) => {
  const array = []
  array[0] = start
  while (array[array.length - 1] < end) {
    array[array.length] = array[array.length - 1] + step
  }
  if (array[array.length - 1] !== end) {
    array[array.length] = end
  }
  return array
}

const getRay = (point, angle, sequence) => {
  const directionVector = getDirectionVector(angle)
  return sequence.map((element) => vectorDoTwo(
    point, directionVector, (a, b) => a + element * b
  ))
}
