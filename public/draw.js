/* global canvas, camera, context, circles, rectangles, player, getDirectionVector, BLACK, FIRE_YELLOW, GREEN, RED, CANVAS_HEIGHT, CANVAS_WIDTH, YELLOW, UPDATE_SECOND, MAXIMUM_SPEED, vectorDoTwo, BLUE, combatants */
/* eslint no-unused-vars: "off" */

const lerp = (drawPosition, realPosition) => {
  return {
    x: 0.5 * drawPosition.x + 0.5 * realPosition.x,
    y: 0.5 * drawPosition.y + 0.5 * realPosition.y
  }
}

const screenify = (vector) => {
  return {
    x: vector.x,
    y: vector.y
  }
}

const drawCircle = (position, radius, color) => {
  const screenPosition = screenify(position)

  context.beginPath()
  context.arc(
    screenPosition.x,
    screenPosition.y,
    radius,
    0,
    2 * Math.PI,
    false
  )
  context.fillStyle = color
  context.fill()
  context.closePath()
}

const drawLine = (start, end, color, stroke) => {
  const screenStart = screenify(start)
  const screenEnd = screenify(end)
  context.beginPath()
  context.moveTo(screenStart.x, screenStart.y)
  context.lineTo(screenEnd.x, screenEnd.y)
  context.strokeStyle = color
  context.lineWidth = stroke
  context.stroke()
  context.closePath()
}

const drawRectangle = (position, width, height, color) => {
  context.fillStyle = color
  context.fillRect(
    position.x - (width / 2),
    position.y - (height / 2),
    width,
    height
  )
}

const resetContext = (context) => {
  context.setTransform(1, 0, 0, 1, 0, 0)
  context.scale(canvas.width / CANVAS_WIDTH, canvas.height / CANVAS_HEIGHT)
}

const draw = () => {
  // Center the camera
  camera.position = camera.target.draw
  camera.angle = camera.target.drawAngle

  // Reset the canvas
  resetContext(context)
  context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT) // Clear the screen

  // Move the context
  context.translate(CANVAS_WIDTH / 2, CANVAS_HEIGHT * 0.9)
  context.rotate(-camera.angle - Math.PI / 2)
  context.translate(-camera.position.x, -camera.position.y)

  // Draw the attack
  if (player.attack.ray.length > 0) {
    if (player.attack.remaining > 37) {
      const colors = {
        40: FIRE_YELLOW,
        39: FIRE_YELLOW,
        38: YELLOW,
        37: YELLOW
      }
      drawLine(
        player.attack.ray[0],
        player.attack.hit.point,
        colors[player.attack.remaining],
        0.5
      )
    }
  }

  // Draw each circle.
  circles.map(circle => {
    drawCircle(circle.draw, circle.radius, circle.color)

    // Draw weapons
    if (circle.ai.name !== 'civilian') {
      const directionVector = getDirectionVector(circle.drawAngle)
      const end = vectorDoTwo(
        circle.draw, directionVector, (a, b) => a + circle.weaponSize * b
      )
      drawLine(circle.draw, end, BLACK, 0.5)
    }

    // Smooth movement
    circle.draw = lerp(circle.draw, circle.position)
    circle.draw = circle.position
    circle.drawAngle = circle.angle
    circle.drawAngle = 0.9 * circle.drawAngle + 0.1 * circle.angle
  })

  // Draw each rectangle
  rectangles.map(rectangle => {
    drawRectangle(rectangle.draw, rectangle.width, rectangle.height, rectangle.color)

    // Smooth movement
    rectangle.draw = lerp(rectangle.draw, rectangle.position)
    rectangle.draw = rectangle.position
  })

  resetContext(context)

  // Draw the cooldown
  const remaining = Math.ceil(player.attack.remaining / UPDATE_SECOND * 10) / 10
  if (remaining === 0) {
    context.fillStyle = GREEN
  } else {
    context.fillStyle = RED
    // Draw the indicator
    const width = 20 * (player.attack.remaining / player.attack.cooldown)
    context.fillRect(20, 6, width, 5)
  }
  context.fillText(remaining, 10, 10)

  // Draw the score
  context.fillStyle = BLACK
  context.fillText(player.score, 151.8, 10)

  window.requestAnimFrame(() => draw())
}
