/* global player */

// Handle keyboard input.
const keys = {
  37: 'left', // left
  38: 'up', // up
  39: 'right', // right
  40: 'down', // down
  65: 'left', // a
  68: 'right', // d
  69: 'strafeRight', // e
  74: 'strafeLeft', // j
  75: 'strafeRight', // k
  79: 'strafeLeft', // o
  80: 'strafeRight', // p
  81: 'strafeLeft', // q
  83: 'down', // s
  87: 'up', // w
  32: 'attack' // space
}
const handleinputKeyEvent = (event, value) => {
  const key = keys[event.keyCode]
  if (key) {
    if (key === 'attack') {
      if (value) {
        player.input.fire = false
        player.input.aim = true
      } else {
        player.input.aim = false
        player.input.fire = true
      }
    }
    player.input[key] = value
  }
}
window.onkeydown = (event) => handleinputKeyEvent(event, true)
window.onkeyup = (event) => handleinputKeyEvent(event, false)
